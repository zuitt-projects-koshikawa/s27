


const http = require("http");
const port = 8000;

// Mock Data

let items = [

	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
]

const server = http.createServer((req, res) => {

   	if (req.url === "/items".toLowerCase() && req.method === "GET") {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(items));
        console.log(items);

	} else if (req.url === "/items".toLowerCase() && req.method === "POST") {
		let requestItem = "";

		// data-step

		req.on('data', (data) => {
			requestItem += data
		})

		// end-step

		req.on('end', () => {
			requestItem = JSON.parse(requestItem)

			let newItem = {

				name: requestItem.name,
				price: requestItem.price,
				isActive: requestItem.isActive
			}

			items.push(newItem);
			console.log(items);

			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(items))
		})
	} else if (req.url === "/items".toLowerCase() && req.method === "DELETE") {

        let requestItem = "";



        req.on('data', (data) => {
			requestItem -= data
        })

        req.on('end', () => {
            requestItem = JSON.parse(requestItem)

            items.pop(requestItem);

            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(items));
            console.log(items)

        })

    }







})

server.listen(port);
console.log(`Server is now accessible at localhost:${port}`)
